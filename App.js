import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image
} from "react-native";
import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator,
  createBottomTabNavigator
} from "react-navigation";
import Cart from "./Navigation/BottomNavigationPages/Cart";
import Categories from "./Navigation/BottomNavigationPages/Categories";
import Custom from "./Navigation/BottomNavigationPages/Custom";
import Home from "./Navigation/BottomNavigationPages/Home";
import Profile from "./Navigation/BottomNavigationPages/Profile";
import Custom3 from "./Navigation/BottomNavigationPages/CustomMeasurments/Custom3";
import Icon from "react-native-vector-icons/FontAwesome";
import CatDescription from "./Navigation/BottomNavigationPages/CatDescription";
import PentCustom1 from "./Navigation/BottomNavigationPages/CustomMeasurments/Pents/PentCustom1";
import PentMeaurments from "./Navigation/BottomNavigationPages/CustomMeasurments/Pents/PentMeaurments";
import Splash from "./Splash";
import Login from "./Login";
import Signup from "./Signup";
import Custom1 from "./Navigation/BottomNavigationPages/CustomMeasurments/Custom1";
import Meaurments from "./Navigation/BottomNavigationPages/CustomMeasurments/Meaurments";
import Custom2 from "./Navigation/BottomNavigationPages/CustomMeasurments/Custom2";
import EditProfile from "./Navigation/BottomNavigationPages/CustomMeasurments/Editprofile";
import Order from "./Navigation/BottomNavigationPages/CustomMeasurments/Order";
import DoneCart from "./Navigation/BottomNavigationPages/DoneCart";
import CartForm from "./Navigation/BottomNavigationPages/CartFrom";
import HelpandSupport from "./Navigation/BottomNavigationPages/CustomMeasurments/HelpandSupport";
import Wish from "./Navigation/BottomNavigationPages/CustomMeasurments/Wish";
import CatList from "./Navigation/BottomNavigationPages/CustomMeasurments/CatList";
export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}
const ProfileStackNavigator = createStackNavigator(
  {
    Profile: Profile,
    EditProfile: EditProfile,
    Order: Order,
    Wish: Wish,
    HelpandSupport: HelpandSupport
    //Login: Login
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);

const HomeStackNavigator = createStackNavigator(
  {
    Home: Home,
    Categories: Categories,
    CatDescription: CatDescription
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);

const CustomStackNavigator = createStackNavigator(
  {
    Custom: Custom,
    Meaurments: Meaurments,
    Custom1: Custom1,
    Custom2: Custom2,
    Custom3: Custom3,
    CartForm: CartForm,
    DoneCart: DoneCart,
    PentCustom1: PentCustom1,
    PentMeaurments: PentMeaurments
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);
const CategoriesStackNavigator = createStackNavigator(
  {
    CatList: CatList,
    Categories: Categories,
    CatDescription: CatDescription
    //Cart: Cart
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);

CustomStackNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

const Bottom = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" size={24} color={tintColor} />
        )
      }
    },
    Categories: {
      screen: CategoriesStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="th-large" size={24} color={tintColor} />
          // <Image
          //   source={require("../img/catg.png")}
          //   style={{ width: 20, height: 20 }}
          // />
        )
      }
    },
    Custom: {
      screen: CustomStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("./img/Sci.png")}
            style={{ width: 25, height: 25 }}
            color={tintColor}
          />
        )
      }
    },
    Cart: {
      screen: Cart,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="cart-arrow-down" size={24} color={tintColor} />
        )
      }
    },
    Profile: {
      screen: ProfileStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="user" size={24} color={tintColor} />
        )
      }
    }
  },
  {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: "#15a7ee",
      inactiveTintColor: "#d1cece",
      showIcon: true
    }
  }
);
const AuthStackNavigator = createStackNavigator({
  Login: Login,
  Signup: Signup,
  Profile: Profile
});
const AppSwitchNavigator = createSwitchNavigator(
  {
    Splash: Splash,
    Auth: AuthStackNavigator,
    App: Bottom
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

const AppContainer = createAppContainer(AppSwitchNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
