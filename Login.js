import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
  ScrollView
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

import axios from "axios";
const ACCESS_TOKEN = "access_token";

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      error: "",
      loading: false,
      hidePassword: true,
      token: ""
    };
  }

  async storeToken() {
    const { token } = this.state;
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, token);
      console.log("Token Saved", token);
      //this.getToken();
    } catch (error) {
      console.log("Token cannot be saved");
    }
  }

  // async getToken() {
  //   try {
  //     let token = await AsyncStorage.getItem(ACCESS_TOKEN);
  //     console.log("Token is", token);
  //     this.checkToken();
  //   } catch (error) {
  //     console.log("Cannot get token");
  //   }
  // }

  // checkToken(){
  //   const {token} = this.state
  //   axios
  //     .post("http://198.245.53.50:5000/api/users/check", {
  //       token: token
  //     })
  //     .then(response => console.log(response));
  // }

  static navigationOptions = {
    header: null
  };
  onLoginButtonPress() {
    const { email, password } = this.state;
    this.setState({ error: "", loading: true });

    axios
      .post("https://customsuits.herokuapp.com/api/login", {
        email: email,
        password: password
      })
      .then(response => {
        console.log(response);
        this.setState({ token: response.data.token });
      })
      //.then(this.storeToken.bind(this))
      .then(this.onLoginSuccess.bind(this))
      .then(() => {
        this.props.navigation.navigate("Home");
      })
      .catch(this.onloginFailure.bind(this));
    //.catch(error => console.log("error from login:", error));
  }

  onLoginSuccess() {
    this.setState({
      email: "",
      password: "",
      loading: false
    });
  }
  onloginFailure() {
    this.setState({ error: "Authentication Failed", loading: false });
    alert("Invalid Email or Password");
  }

  toggleShowButton() {
    this.setState({ hidePassword: !this.state.hidePassword });
  }

  toggleShow;

  renderButton() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" color="#1be033" />;
    } else {
      return (
        <TouchableOpacity
          onPress={this.onLoginButtonPress.bind(this)}
          //onPress={() => this.props.navigation.navigate("Home")}
        >
          <ImageBackground
            source={require("./img/loginButton.png")}
            style={{ width: 280, height: 50 }}
          />
        </TouchableOpacity>
      );
    }
  }

  render() {
    return (
      <ImageBackground
        style={{ backgroundColor: "white", width: "100%", height: "100%" }}
      >
        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.loginTextStyle}>Sign In</Text>
          </View>

          <View style={styles.textInputStyle}>
            <View
              style={{
                flexDirection: "row",
                borderColor: "#15a7ee",
                borderWidth: 1,
                borderRadius: 25,
                height: 51,
                width: "80%",
                backgroundColor: "rgba(255, 255, 255, 0.3)"
              }}
            >
              <Icon
                name="ios-mail"
                size={25}
                style={{
                  color: "#15a7ee",
                  paddingLeft: 15,
                  paddingTop: 13,
                  paddingRight: 10
                }}
              />
              <TextInput
                fontSize={20}
                placeholder="example@gmail.com"
                autoCorrect={false}
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
                placeholderTextColor="#15a7ee"
                style={{ color: "#15a7ee", height: "100%", width: "75%" }}
                // selectionColor="white"
                // underlineColorAndroid="white"
                autoCapitalize="none"
              />
            </View>
            {/* <TouchableOpacity
              onPress={this.toggleShowButton.bind(this)}
              style={styles.showButton}
            >
              <Text style={{ color: "#15a7ee" }}>
                {this.state.hidePassword ? "Show" : "Hide"}
              </Text>
            </TouchableOpacity> */}

            {/* <TextInput
              fontSize={20}
              placeholder="Password"
              placeholderTextColor="#15a7ee"
              secureTextEntry={this.state.hidePassword}
              onChangeText={password => this.setState({ password })}
              value={this.state.password}
              underlineColorAndroid="#15a7ee"
              style={{ color: "#15a7ee", height: 50, width: "60%" }}
            /> */}
            <View
              style={{
                flexDirection: "row",
                borderColor: "#15a7ee",
                borderWidth: 1,
                borderRadius: 25,
                height: 51,
                width: "80%",
                marginTop: 10,
                backgroundColor: "rgba(255, 255, 255, 0.3)"
              }}
            >
              <Icon
                name="ios-lock"
                size={25}
                style={{
                  color: "#15a7ee",
                  paddingLeft: 15,
                  paddingTop: 12,
                  paddingRight: 10
                }}
              />
              <TextInput
                fontSize={20}
                placeholder="Password"
                secureTextEntry={this.state.hidePassword}
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
                placeholderTextColor="#15a7ee"
                style={{ color: "#15a7ee", height: "100%", width: "70%" }}
                // selectionColor="white"
                // underlineColorAndroid="white"
                autoCapitalize="none"
              />
              <TouchableOpacity
                onPress={this.toggleShowButton.bind(this)}
                style={styles.showButton}
              >
                <Icon
                  style={{ color: "#15a7ee" }}
                  name={this.state.hidePassword ? "ios-eye" : "ios-eye-off"}
                  size={25}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.forgotCreateStyle}>
            <Text
              style={styles.textStyle}
              //  onPress={() => this.props.navigation.navigate("Signup")}
            >
              Forgot Password?
            </Text>
          </View>
          <View style={styles.buttonsViewStyle}>
            {this.renderButton()}
            <TouchableOpacity
              style={{ marginTop: 20 }}
              onPress={() => this.props.navigation.navigate("Signup")}
            >
              <ImageBackground
                source={require("./img/createAccountButton.png")}
                style={{ width: 280, height: 50 }}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 100
  },
  textStyle: {
    fontSize: 13,
    fontWeight: "bold",
    color: "#15a7ee"
  },
  loginTextStyle: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#15a7ee"
  },
  input: {
    backgroundColor: "#15a7ee",
    width: 40,
    height: 40,
    marginHorizontal: 20,
    paddingLeft: 45,
    borderRadius: 20,
    color: "#15a7ee"
  },
  showButton: {
    paddingTop: 12,
    paddingLeft: 10
  },
  textInputStyle: {
    alignItems: "center",
    flexDirection: "column",
    marginTop: 80
  },
  forgotCreateStyle: {
    //alignItems: "center",
    //flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
    marginLeft: 210
  },
  buttonsViewStyle: {
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: 50
  }
});
