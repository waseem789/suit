// import React, { Component } from "react";
// import { Text, View, Image } from "react-native";
// import Cart from "./BottomNavigationPages/Cart";
// import Categories from "./BottomNavigationPages/Categories";
// import Custom from "./BottomNavigationPages/Custom";
// import Home from "./BottomNavigationPages/Home";
// import Profile from "./BottomNavigationPages/Profile";
// import Custom3 from "./BottomNavigationPages/CustomMeasurments/Custom3";
// import Icon from "react-native-vector-icons/FontAwesome";
// import CatDescription from "./BottomNavigationPages/CatDescription";
// import PentCustom1 from "./BottomNavigationPages/CustomMeasurments/Pents/PentCustom1";
// import PentMeaurments from "../Navigation/BottomNavigationPages/CustomMeasurments/Pents/PentMeaurments";
// import {
//   createBottomTabNavigator,
//   createAppContainer,
//   createSwitchNavigator,
//   StackNavigator,
//   createStackNavigator
// } from "react-navigation";
// import Custom1 from "./BottomNavigationPages/CustomMeasurments/Custom1";
// import Meaurments from "./BottomNavigationPages/CustomMeasurments/Meaurments";
// import Custom2 from "./BottomNavigationPages/CustomMeasurments/Custom2";
// import EditProfile from "./BottomNavigationPages/CustomMeasurments/Editprofile";
// import Order from "./BottomNavigationPages/CustomMeasurments/Order";
// import DoneCart from "./BottomNavigationPages/DoneCart";
// import CartForm from "./BottomNavigationPages/CartFrom";
// import HelpandSupport from "./BottomNavigationPages/CustomMeasurments/HelpandSupport";
// import Wish from "./BottomNavigationPages/CustomMeasurments/Wish";
// import CatList from "./BottomNavigationPages/CustomMeasurments/CatList";
// import Login from "../Login";
// export default class BottomTab extends Component {
//   render() {
//     return <BottomContainer />;
//   }
// }
// const ProfileStackNavigator = createStackNavigator(
//   {
//     Profile: Profile,
//     EditProfile: EditProfile,
//     Order: Order,
//     Wish: Wish,
//     HelpandSupport: HelpandSupport
//     //Login: Login
//   },
//   {
//     defaultNavigationOptions: {
//       header: null
//     }
//   }
// );

// const HomeStackNavigator = createStackNavigator(
//   {
//     Home: Home,
//     CatDescription: CatDescription
//   },
//   {
//     defaultNavigationOptions: {
//       header: null
//     }
//   }
// );

// const CustomStackNavigator = createStackNavigator(
//   {
//     Custom: Custom,
//     Meaurments: Meaurments,
//     Custom1: Custom1,
//     Custom2: Custom2,
//     Custom3: Custom3,
//     CartForm: CartForm,
//     DoneCart: DoneCart,
//     PentCustom1: PentCustom1,
//     PentMeaurments: PentMeaurments
//   },
//   {
//     defaultNavigationOptions: {
//       header: null
//     }
//   }
// );
// const CategoriesStackNavigator = createStackNavigator(
//   {
//     CatList: CatList,
//     Categories: Categories,
//     CatDescription: CatDescription
//     //Cart: Cart
//   },
//   {
//     defaultNavigationOptions: {
//       header: null
//     }
//   }
// );

// CustomStackNavigator.navigationOptions = ({ navigation }) => {
//   let tabBarVisible = true;
//   if (navigation.state.index > 0) {
//     tabBarVisible = false;
//   }

//   return {
//     tabBarVisible
//   };
// };

// const Bottom = createBottomTabNavigator(
//   {
//     Home: {
//       screen: HomeStackNavigator,
//       navigationOptions: {
//         tabBarIcon: ({ tintColor }) => (
//           <Icon name="home" size={24} color={tintColor} />
//         )
//       }
//     },
//     Categories: {
//       screen: CategoriesStackNavigator,
//       navigationOptions: {
//         tabBarIcon: ({ tintColor }) => (
//           <Icon name="th-large" size={24} color={tintColor} />
//           // <Image
//           //   source={require("../img/catg.png")}
//           //   style={{ width: 20, height: 20 }}
//           // />
//         )
//       }
//     },
//     Custom: {
//       screen: CustomStackNavigator,
//       navigationOptions: {
//         tabBarIcon: ({ tintColor }) => (
//           <Image
//             source={require("../img/Sci.png")}
//             style={{ width: 25, height: 25 }}
//             color={tintColor}
//           />
//         )
//       }
//     },
//     Cart: {
//       screen: Cart,
//       navigationOptions: {
//         tabBarIcon: ({ tintColor }) => (
//           <Icon name="cart-arrow-down" size={24} color={tintColor} />
//         )
//       }
//     },
//     Profile: {
//       screen: ProfileStackNavigator,
//       navigationOptions: {
//         tabBarIcon: ({ tintColor }) => (
//           <Icon name="user" size={24} color={tintColor} />
//         )
//       }
//     }
//   },
//   {
//     tabBarOptions: {
//       showLabel: false,
//       activeTintColor: "#15a7ee",
//       inactiveTintColor: "#d1cece",
//       showIcon: true
//     }
//   }
// );

// const BottomContainer = createAppContainer(Bottom);
