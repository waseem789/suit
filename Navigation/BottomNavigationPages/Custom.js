import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Dimensions,
  StyleSheet,
  TouchableHighlight,
  StatusBar
} from "react-native";
import { Header, Left, Right, Body, Title } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
//import IconGa from "react-native-vector-icons/MaterialCommunityIcons";
// import IconFa from "react-native-vector-icons/SimpleLineIcons";
import Iconua from "react-native-vector-icons/Ionicons";

export default class Custom extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* <View
          style={{
            height: 0.3,
            borderWidth: 0.3,
            borderColor: "white",
            width: "100%"
          }}
        /> */}
        <Header
          style={{
            backgroundColor: "#15a7ee"
          }}
        >
          <StatusBar backgroundColor="#007bb7" barStyle="light-content" />

          <Left>
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                color: "white",
                width: 200
              }}
            >
              Cutomize Your Suit
            </Text>
          </Left>
        </Header>

        <ScrollView>
          <View
            style={{
              flexDirection: "column",
              marginLeft: 9,
              marginTop: 5,
              marginRight: 9
            }}
          >
            <View style={{ flexDirection: "row", width: "100%" }}>
              <View
                style={{
                  height: 200,
                  width: "50%",
                  backgroundColor: "white",
                  marginTop: 10,
                  marginRight: 0,
                  paddingRight: 5,
                  borderColor: "grey",
                  borderWidth: 0.5,
                  marginLeft: 2,
                  borderRadius: 5
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Meaurments")}
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/1.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="cut"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
                <Text
                  style={{
                    paddingLeft: 12,
                    fontSize: 15,
                    color: "black",
                    fontWeight: "bold",
                    paddingTop: 30
                  }}
                >
                  Chontaduro Barcelona
                </Text>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    borderColor: "#d0dbd6",
                    borderWidth: 0.5,
                    marginTop: 3,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginLeft: 3,
                    marginRight: 3
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "black" }}>Price:</Text>
                    <Text>$200</Text>
                  </View>
                  <Text style={{ color: "black" }}>20% Off</Text>
                </View>
              </View>

              <View
                style={{
                  height: 200,
                  width: "50%",
                  backgroundColor: "white",
                  marginTop: 10,
                  marginRight: 0,
                  paddingRight: 5,
                  borderColor: "grey",
                  borderWidth: 0.5,
                  marginLeft: 2,
                  borderRadius: 5
                }}
              >
                <ImageBackground
                  style={{
                    width: 165,
                    padding: 10,
                    height: 120,
                    marginTop: 0,
                    marginRight: 5
                  }}
                  source={require("../../img/2.jpg")}
                  imageStyle={{ borderRadius: 5 }}
                >
                  <View
                    style={{
                      position: "absolute",
                      width: 40,
                      height: 40,
                      borderRadius: 20,
                      backgroundColor: "white",
                      left: 8,
                      top: 100,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      name="cut"
                      size={25}
                      style={{ color: "#15a7ee", borderRadius: 20 }}
                    />
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    paddingLeft: 12,
                    fontSize: 15,
                    color: "black",
                    fontWeight: "bold",
                    paddingTop: 30
                  }}
                >
                  Chontaduro Barcelona
                </Text>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    borderColor: "#d0dbd6",
                    borderWidth: 0.5,
                    marginTop: 3,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginLeft: 3,
                    marginRight: 3
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "black" }}>Price:</Text>
                    <Text>$200</Text>
                  </View>
                  <Text style={{ color: "#15a7ee" }}>Trending</Text>
                </View>
              </View>
            </View>

            <View style={{ flexDirection: "row", width: "100%" }}>
              <View
                style={{
                  height: 200,
                  width: "50%",
                  backgroundColor: "white",
                  marginTop: 10,
                  marginRight: 0,
                  paddingRight: 5,
                  borderColor: "grey",
                  borderWidth: 0.5,
                  marginLeft: 2,
                  borderRadius: 5
                }}
              >
                <ImageBackground
                  style={{
                    width: 165,
                    padding: 10,
                    height: 120,
                    marginTop: 0,
                    marginRight: 5
                  }}
                  source={require("../../img/3.jpg")}
                  imageStyle={{ borderRadius: 5 }}
                >
                  <View
                    style={{
                      position: "absolute",
                      width: 40,
                      height: 40,
                      borderRadius: 20,
                      backgroundColor: "white",
                      left: 8,
                      top: 100,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      name="cut"
                      size={25}
                      style={{ color: "#15a7ee", borderRadius: 20 }}
                    />
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    paddingLeft: 12,
                    fontSize: 15,
                    color: "black",
                    fontWeight: "bold",
                    paddingTop: 30
                  }}
                >
                  Chontaduro Barcelona
                </Text>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    borderColor: "#d0dbd6",
                    borderWidth: 0.5,
                    marginTop: 3,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginLeft: 3,
                    marginRight: 3
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "black" }}>Price:</Text>
                    <Text>$200</Text>
                  </View>
                  <Text style={{ color: "#15a7ee" }}>Trending</Text>
                </View>
              </View>
              <View
                style={{
                  height: 200,
                  width: "50%",
                  backgroundColor: "white",
                  marginTop: 10,
                  marginRight: 0,
                  paddingRight: 5,
                  borderColor: "grey",
                  borderWidth: 0.5,
                  marginLeft: 2,
                  borderRadius: 5
                }}
              >
                <ImageBackground
                  style={{
                    width: 165,
                    padding: 10,
                    height: 120,
                    marginTop: 0,
                    marginRight: 5
                  }}
                  source={require("../../img/4.jpg")}
                  imageStyle={{ borderRadius: 5 }}
                >
                  <View
                    style={{
                      position: "absolute",
                      width: 40,
                      height: 40,
                      borderRadius: 20,
                      backgroundColor: "white",
                      left: 8,
                      top: 100,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      name="cut"
                      size={25}
                      style={{ color: "#15a7ee", borderRadius: 20 }}
                    />
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    paddingLeft: 12,
                    fontSize: 15,
                    color: "black",
                    fontWeight: "bold",
                    paddingTop: 30
                  }}
                >
                  Chontaduro Barcelona
                </Text>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    borderColor: "#d0dbd6",
                    borderWidth: 0.5,
                    marginTop: 3,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginLeft: 3,
                    marginRight: 3
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "black" }}>Price:</Text>
                    <Text>$200</Text>
                  </View>
                  <Text style={{ color: "black" }}>20% Off</Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "row", width: "100%" }}>
              <View
                style={{
                  height: 200,
                  width: "50%",
                  backgroundColor: "white",
                  marginTop: 10,
                  marginRight: 0,
                  paddingRight: 5,
                  borderColor: "grey",
                  borderWidth: 0.5,
                  marginLeft: 2,
                  borderRadius: 5
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("PentMeaurments")
                  }
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/5.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="cut"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
                <Text
                  style={{
                    paddingLeft: 12,
                    fontSize: 15,
                    color: "black",
                    fontWeight: "bold",
                    paddingTop: 30
                  }}
                >
                  Chontaduro Barcelona
                </Text>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    borderColor: "#d0dbd6",
                    borderWidth: 0.5,
                    marginTop: 3,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginLeft: 3,
                    marginRight: 3
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "black" }}>Price:</Text>
                    <Text>$200</Text>
                  </View>
                  <Text style={{ color: "#15a7ee" }}>Trending</Text>
                </View>
              </View>
              <View
                style={{
                  height: 200,
                  width: "50%",
                  backgroundColor: "white",
                  marginTop: 10,
                  marginRight: 0,
                  paddingRight: 5,
                  borderColor: "grey",
                  borderWidth: 0.5,
                  marginLeft: 2,
                  borderRadius: 5
                }}
              >
                <ImageBackground
                  style={{
                    width: 165,
                    padding: 10,
                    height: 120,
                    marginTop: 0,
                    marginRight: 5
                  }}
                  source={require("../../img/6.jpg")}
                  imageStyle={{ borderRadius: 5 }}
                >
                  <View
                    style={{
                      position: "absolute",
                      width: 40,
                      height: 40,
                      borderRadius: 20,
                      backgroundColor: "white",
                      left: 8,
                      top: 100,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      name="cut"
                      size={25}
                      style={{ color: "#15a7ee", borderRadius: 20 }}
                    />
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    paddingLeft: 12,
                    fontSize: 15,
                    color: "black",
                    fontWeight: "bold",
                    paddingTop: 30
                  }}
                >
                  Chontaduro Barcelona
                </Text>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    borderColor: "#d0dbd6",
                    borderWidth: 0.5,
                    marginTop: 3,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginLeft: 3,
                    marginRight: 3
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "black" }}>Price:</Text>
                    <Text>$200</Text>
                  </View>
                  <Text style={{ color: "black" }}>20% Off</Text>
                </View>
              </View>
            </View>
            <View style={{ flexDirection: "row", width: "100%" }}>
              <View
                style={{
                  height: 200,
                  width: "50%",
                  backgroundColor: "white",
                  marginTop: 10,
                  marginRight: 0,
                  paddingRight: 5,
                  borderColor: "grey",
                  borderWidth: 0.5,
                  marginLeft: 2,
                  borderRadius: 5
                }}
              >
                <ImageBackground
                  style={{
                    width: 165,
                    padding: 10,
                    height: 120,
                    marginTop: 0,
                    marginRight: 5
                  }}
                  source={require("../../img/4.jpg")}
                  imageStyle={{ borderRadius: 5 }}
                >
                  <View
                    style={{
                      position: "absolute",
                      width: 40,
                      height: 40,
                      borderRadius: 20,
                      backgroundColor: "white",
                      left: 8,
                      top: 100,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      name="cut"
                      size={25}
                      style={{ color: "#15a7ee", borderRadius: 20 }}
                    />
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    paddingLeft: 12,
                    fontSize: 15,
                    color: "black",
                    fontWeight: "bold",
                    paddingTop: 30
                  }}
                >
                  Chontaduro Barcelona
                </Text>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    borderColor: "#d0dbd6",
                    borderWidth: 0.5,
                    marginTop: 3,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginLeft: 3,
                    marginRight: 3
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "black" }}>Price:</Text>
                    <Text>$200</Text>
                  </View>
                  <Text style={{ color: "black" }}>20% Off</Text>
                </View>
              </View>
              <View
                style={{
                  height: 200,
                  width: "50%",
                  backgroundColor: "white",
                  marginTop: 10,
                  marginRight: 0,
                  paddingRight: 5,
                  borderColor: "grey",
                  borderWidth: 0.5,
                  marginLeft: 2,
                  borderRadius: 5
                }}
              >
                <ImageBackground
                  style={{
                    width: 165,
                    padding: 10,
                    height: 120,
                    marginTop: 0,
                    marginRight: 5
                  }}
                  source={require("../../img/2.jpg")}
                  imageStyle={{ borderRadius: 5 }}
                >
                  <View
                    style={{
                      position: "absolute",
                      width: 40,
                      height: 40,
                      borderRadius: 20,
                      backgroundColor: "white",
                      left: 8,
                      top: 100,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      name="cut"
                      size={25}
                      style={{ color: "#15a7ee", borderRadius: 20 }}
                    />
                  </View>
                </ImageBackground>
                <Text
                  style={{
                    paddingLeft: 12,
                    fontSize: 15,
                    color: "black",
                    fontWeight: "bold",
                    paddingTop: 30
                  }}
                >
                  Chontaduro Barcelona
                </Text>

                <View
                  style={{
                    width: "100%",
                    height: 1,
                    borderColor: "#d0dbd6",
                    borderWidth: 0.5,
                    marginTop: 3,
                    flexDirection: "row"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginLeft: 3,
                    marginRight: 3
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ color: "black" }}>Price:</Text>
                    <Text>$200</Text>
                  </View>
                  <Text style={{ color: "black" }}>20% Off</Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  submit: {
    height: 25,
    width: 130,
    backgroundColor: "#3cc0ff",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#fff"
  },
  submitText: {
    color: "#fff",
    paddingLeft: 10
  }
});
