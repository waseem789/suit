import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  StatusBar
} from "react-native";
import { Header, Left, Right, Body, Title } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
width = Dimensions.get("window").width;

export default class Chats extends Component {
  render() {
    return (
      <ScrollView>
        <StatusBar backgroundColor="#15a7ee" barStyle="light-content" />
        <View style={{ backgroundColor: "white" }}>
          <View style={{ backgroundColor: "white" }}>
            <View style={{ backgroundColor: "white" }}>
              {/* <View
                style={{
                  height: 0.3,
                  borderWidth: 0.3,
                  borderColor: "white",
                  width: "100%"
                }}
              /> */}
              <Header
                style={{
                  backgroundColor: "#15a7ee"
                }}
              >
                <StatusBar backgroundColor="#007bb7" barStyle="light-content" />

                <Left>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: "bold",
                      color: "white",
                      width: 200
                    }}
                  >
                    Account Details
                  </Text>
                </Left>
              </Header>

              <View
                style={{
                  height: 0.5,
                  width: width,
                  borderWidth: 0.5,
                  borderColor: "grey"
                }}
              />

              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: 20
                }}
              >
                <Image
                  source={require("../../img/2.jpg")}
                  style={{
                    width: 150,
                    height: 150,
                    borderRadius: 80,
                    borderWidth: 2,
                    borderColor: "grey"
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginTop: 10,
                  backgroundColor: "white"
                }}
              >
                <Image
                  source={require("../../img/person.png")}
                  style={{ width: 50, height: 50, marginRight: 10 }}
                />
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("EditProfile")}
                >
                  <View>
                    <Text style={styles.text}>Edit Profile</Text>
                    <Text style={styles.normalText}>
                      Edit Your Profile Here !
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: 0.2,
                  width: width,
                  borderWidth: 0.2,
                  borderColor: "grey"
                }}
              />

              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <Icon
                  name="list-alt"
                  size={40}
                  style={{
                    marginLeft: 10,
                    marginRight: 8,
                    marginTop: 10,
                    color: "black"
                  }}
                />
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Order")}
                >
                  <View>
                    <Text style={styles.text}>Orders</Text>
                    <Text style={styles.normalText}>
                      Find out what's going on !
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: 0.2,
                  width: width,
                  borderWidth: 0.2,
                  borderColor: "grey"
                }}
              />

              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <Icon
                  name="heart"
                  size={40}
                  style={{
                    marginLeft: 10,
                    marginRight: 5,
                    marginTop: 10,
                    color: "black"
                  }}
                />
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Wish")}
                >
                  <View>
                    <Text style={styles.text}>WishList</Text>
                    <Text style={styles.normalText}>
                      All your desire wishlist are here !
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  height: 0.2,
                  width: width,
                  borderWidth: 0.2,
                  borderColor: "grey"
                }}
              />
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("HelpandSupport")}
              >
                <View style={{ flexDirection: "row", marginTop: 10 }}>
                  <Icon
                    name="question-circle"
                    size={45}
                    style={{
                      marginLeft: 10,
                      marginRight: 10,
                      marginTop: 10,
                      color: "black"
                    }}
                  />
                  <View>
                    <Text style={styles.text}>Help and Support</Text>
                    <Text style={styles.normalText}>
                      Help center and legal terms !
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>

              <View
                style={{
                  height: 0.2,
                  width: width,
                  borderWidth: 0.2,
                  borderColor: "grey"
                }}
              />
            </View>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <Icon
                  name="sign-out"
                  size={40}
                  style={{
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 10,
                    color: "black"
                  }}
                />
                <View>
                  <Text style={styles.text}>Logout</Text>
                  <Text style={styles.normalText}>For Logout !</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  headText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 20,
    margin: 15
  },
  text: {
    color: "black",
    fontWeight: "bold",
    fontSize: 15,
    marginLeft: 20,
    marginTop: 10
  },
  normalText: {
    color: "black",
    fontSize: 10,
    marginLeft: 20,
    marginTop: 8,
    marginBottom: 15
  }
});
