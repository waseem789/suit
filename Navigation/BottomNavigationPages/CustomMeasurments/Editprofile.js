import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  ScrollView,
  Image
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import { TextInput } from "react-native-paper";
width = Dimensions.get("window").width;
height = Dimensions.get("window").height;
export default class EditProfile extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            height: 0.3,
            borderWidth: 0.3,
            borderColor: "white",
            width: "100%"
          }}
        />
        <Header
          style={{
            backgroundColor: "#15a7ee"
          }}
        >
          <StatusBar backgroundColor="#15a7ee" barStyle="light-content" />

          <Left>
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                color: "white",
                width: 200,
                marginLeft: 20
              }}
            >
              EditProfile
            </Text>
          </Left>
        </Header>

        <ScrollView>
          <View
            style={{
              height: 0.5,
              width: width,
              borderWidth: 0.5,
              borderColor: "grey",
              marginBottom: 20
            }}
          />
          <View
            style={{
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              source={require("../../../img/2.jpg")}
              style={{
                width: 150,
                height: 150,
                borderRadius: 80,
                borderWidth: 2,
                borderColor: "grey"
              }}
            />
          </View>
          <View
            style={{
              width: width - 30,
              height: 30,
              marginRight: 15,
              marginLeft: 15,
              marginTop: 20,
              marginBottom: 20,
              borderRadius: 20
            }}
          >
            <TouchableOpacity>
              <TextInput
                label="Name"
                mode="outlined"
                editable="true"
                theme={{
                  colors: {
                    placeholder: "grey",
                    background: "#f5f6f5",
                    text: "grey",
                    primary: "#5d5d5d"
                  }
                }}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: width - 30,
              height: 30,
              marginRight: 15,
              marginLeft: 15,
              marginBottom: 20,
              marginTop: 20
            }}
          >
            <TouchableOpacity>
              <TextInput
                label="Phone"
                mode="outlined"
                editable="true"
                theme={{
                  colors: {
                    placeholder: "grey",
                    background: "#f5f6f5",
                    text: "grey",
                    primary: "#5d5d5d"
                  }
                }}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: width - 30,
              height: 30,
              marginRight: 15,
              marginLeft: 15,
              marginBottom: 20,
              marginTop: 20
            }}
          >
            <TouchableOpacity>
              <TextInput
                label="City"
                mode="outlined"
                editable="true"
                theme={{
                  colors: {
                    placeholder: "grey",
                    background: "#f5f6f5",
                    text: "grey",
                    primary: "#5d5d5d"
                  }
                }}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: width - 30,
              height: 30,
              marginRight: 15,
              marginLeft: 15,
              marginBottom: 20,
              marginTop: 20
            }}
          >
            <TouchableOpacity>
              <TextInput
                label="Address"
                mode="outlined"
                editable="true"
                theme={{
                  colors: {
                    placeholder: "grey",
                    background: "#f5f6f5",
                    text: "grey",
                    primary: "#5d5d5d"
                  }
                }}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: width - 30,
              height: 30,
              marginRight: 15,
              marginLeft: 15,
              marginTop: 20,
              marginBottom: 50
            }}
          >
            <TouchableOpacity style={{ borderColor: "grey" }}>
              <TextInput
                label="Age"
                mode="outlined"
                editable="true"
                theme={{
                  colors: {
                    placeholder: "grey",
                    background: "#f5f6f5",
                    text: "grey",
                    primary: "#5d5d5d"
                  }
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <TouchableOpacity
              style={{
                width: 90,
                height: 50,
                borderRadius: 5,
                backgroundColor: "#3cc0ff",
                marginBottom: 10
              }}
              onPress={() => this.props.navigation.navigate("Meaurments")}
            >
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: "bold",
                  marginLeft: 15,
                  color: "white",
                  marginTop: 15,
                  paddingLeft: 5
                }}
              >
                Update
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  headText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 20,
    margin: 15
  }
});
