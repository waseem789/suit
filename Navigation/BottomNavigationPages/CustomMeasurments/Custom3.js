import React, { Component } from "react";
import { TouchableOpacity, View, Image, StatusBar } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text
} from "native-base";
export default class Custom3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderView: false,
      renderView1: false,
      renderView2: false,
      renderView3: false
    };
  }

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#15a7ee" barStyle="light-content" />
        <Content>
          <View>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  renderView: true,
                  renderView1: false,
                  renderView2: false,
                  renderView3: false
                })
              }
            >
              <View
                style={{
                  width: 300,
                  height: 250,
                  marginLeft: 40,
                  marginTop: 10,
                  borderWidth: this.state.renderView ? 3 : 0,
                  borderColor: this.state.renderView ? "#15a7ee" : "black"
                }}
              >
                <Image
                  source={require("../../../img/suits-4.png")}
                  style={{ width: "99%", height: 150, resizeMode: "center" }}
                />
                <View>
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginBottom: 5,
                      paddingTop: 15,
                      marginLeft: 100
                    }}
                  >
                    New Variety
                  </Text>
                  <Text style={{ marginBottom: 5, marginLeft: 50 }}>
                    Works well with your desire{" "}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  renderView: false,
                  renderView1: true,
                  renderView2: false,
                  renderView3: false
                })
              }
            >
              <View
                style={{
                  width: 300,
                  height: 250,
                  marginLeft: 40,
                  marginTop: 10,
                  borderWidth: this.state.renderView1 ? 3 : 0,
                  borderColor: this.state.renderView1 ? "#15a7ee" : "black"
                }}
              >
                <Image
                  source={require("../../../img/suits-5.png")}
                  style={{ width: "99%", height: 150, resizeMode: "center" }}
                />
                <View>
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginBottom: 5,
                      paddingTop: 15,
                      marginLeft: 100
                    }}
                  >
                    New Variety
                  </Text>
                  <Text style={{ marginBottom: 5, marginLeft: 50 }}>
                    Works well with your desire{" "}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  renderView: false,
                  renderView1: false,
                  renderView2: true,
                  renderView3: false
                })
              }
            >
              <View
                style={{
                  width: 300,
                  height: 250,
                  marginLeft: 40,
                  marginTop: 10,
                  borderWidth: this.state.renderView2 ? 3 : 0,
                  borderColor: this.state.renderView2 ? "#15a7ee" : "black"
                }}
              >
                <Image
                  source={require("../../../img/suits-6.png")}
                  style={{ width: "99%", height: 150, resizeMode: "center" }}
                />
                <View>
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginBottom: 5,
                      paddingTop: 15,
                      marginLeft: 100
                    }}
                  >
                    New Variety
                  </Text>
                  <Text style={{ marginBottom: 5, marginLeft: 50 }}>
                    Works well with your desire{" "}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  renderView: false,
                  renderView1: false,
                  renderView2: false,
                  renderView3: true
                })
              }
            >
              <View
                style={{
                  width: 300,
                  height: 250,
                  marginLeft: 40,
                  marginTop: 10,
                  borderWidth: this.state.renderView3 ? 3 : 0,
                  borderColor: this.state.renderView3 ? "#15a7ee" : "black"
                }}
              >
                <Image
                  source={require("../../../img/suits-7.png")}
                  style={{ width: "99%", height: 150, resizeMode: "center" }}
                />
                <View>
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginBottom: 5,
                      paddingTop: 15,
                      marginLeft: 100
                    }}
                  >
                    New Variety
                  </Text>
                  <Text style={{ marginBottom: 5, marginLeft: 50 }}>
                    Works well with your desire{" "}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </Content>
        <Footer
          style={{
            borderStyle: "dashed",
            borderTopWidth: 1,
            borderColor: "black",
            borderRadius: 5
          }}
        >
          <FooterTab style={{ backgroundColor: "white" }}>
            <View>
              <Image
                source={require("../../../img/up.png")}
                style={{
                  width: 80,
                  height: 40,
                  position: "absolute",
                  marginLeft: 290,
                  bottom: 48,
                  resizeMode: "center"
                }}
              />

              <Text style={{ fontSize: 15, paddingTop: 5, color: "black" }}>
                Free shipping and returns.
              </Text>
            </View>

            <TouchableOpacity
              style={{
                backgroundColor: "#15a7ee",
                width: 80,
                height: 40,
                marginRight: 10,
                marginLeft: 10,
                marginTop: 5,
                marginBottom: 5
              }}
              onPress={() => this.props.navigation.navigate("CartForm")}
            >
              <Text
                style={{
                  marginTop: 8,
                  marginLeft: 20,
                  borderRadius: 5,
                  color: "white"
                }}
              >
                NEXT
              </Text>
            </TouchableOpacity>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
