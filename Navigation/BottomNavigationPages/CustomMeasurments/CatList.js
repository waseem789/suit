import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  ScrollView
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";

export default class CatList extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "white"
        }}
      >
        <Header
          style={{
            backgroundColor: "#15a7ee"
          }}
        >
          <StatusBar backgroundColor="#007bb7" barStyle="light-content" />

          <Left>
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                color: "white",
                width: 200,
                marginLeft: 20
              }}
            >
              Categories
            </Text>
          </Left>
        </Header>
        <ScrollView>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
              marginTop: 20
            }}
          >
            <TouchableOpacity
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 2,
                width: 160,
                height: 160
              }}
              onPress={() => this.props.navigation.navigate("Categories")}
            >
              <ImageBackground
                source={require("../../../img/5.jpg")}
                style={{
                  width: 160,
                  height: 160
                }}
                imageStyle={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 90,
                    width: "100%",
                    height: 30,
                    backgroundColor: "rgba(255, 255, 255, 0.3)"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold"
                    }}
                  >
                    Custom Suits
                  </Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 2,
                width: 160,
                height: 160
              }}
            >
              <ImageBackground
                source={require("../../../img/5.jpg")}
                style={{
                  width: 160,
                  height: 160
                }}
                imageStyle={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 90,
                    width: "100%",
                    height: 30,
                    backgroundColor: "rgba(255, 255, 255, 0.3)"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold"
                    }}
                  >
                    Blazzer/Jackets
                  </Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
              marginTop: 20
            }}
          >
            <TouchableOpacity
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 2,
                width: 160,
                height: 160
              }}
            >
              <ImageBackground
                source={require("../../../img/5.jpg")}
                style={{
                  width: 160,
                  height: 160
                }}
                imageStyle={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 90,
                    width: "100%",
                    height: 30,
                    backgroundColor: "rgba(255, 255, 255, 0.3)"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold"
                    }}
                  >
                    Custom Suits
                  </Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 2,
                width: 160,
                height: 160
              }}
            >
              <ImageBackground
                source={require("../../../img/5.jpg")}
                style={{
                  width: 160,
                  height: 160
                }}
                imageStyle={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 90,
                    width: "100%",
                    height: 30,
                    backgroundColor: "rgba(255, 255, 255, 0.3)"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold"
                    }}
                  >
                    Blazzer/Jackets
                  </Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
              marginTop: 20
            }}
          >
            <TouchableOpacity
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 2,
                width: 160,
                height: 160
              }}
            >
              <ImageBackground
                source={require("../../../img/5.jpg")}
                style={{
                  width: 160,
                  height: 160
                }}
                imageStyle={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 90,
                    width: "100%",
                    height: 30,
                    backgroundColor: "rgba(255, 255, 255, 0.3)"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold"
                    }}
                  >
                    Pants
                  </Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 2,
                width: 160,
                height: 160
              }}
            >
              <ImageBackground
                source={require("../../../img/5.jpg")}
                style={{
                  width: 160,
                  height: 160
                }}
                imageStyle={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 90,
                    width: "100%",
                    height: 30,
                    backgroundColor: "rgba(255, 255, 255, 0.3)"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold"
                    }}
                  >
                    Shirts
                  </Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
              marginTop: 20
            }}
          >
            <TouchableOpacity
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 2,
                width: 160,
                height: 160
              }}
            >
              <ImageBackground
                source={require("../../../img/5.jpg")}
                style={{
                  width: 160,
                  height: 160
                }}
                imageStyle={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 90,
                    width: "100%",
                    height: 30,
                    backgroundColor: "rgba(255, 255, 255, 0.3)"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold"
                    }}
                  >
                    Custom Suits
                  </Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                shadowColor: "#000",
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.5,
                shadowRadius: 2,
                elevation: 2,
                width: 160,
                height: 160
              }}
            >
              <ImageBackground
                source={require("../../../img/5.jpg")}
                style={{
                  width: 160,
                  height: 160
                }}
                imageStyle={{ borderRadius: 25 }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 90,
                    width: "100%",
                    height: 30,
                    backgroundColor: "rgba(255, 255, 255, 0.3)"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold"
                    }}
                  >
                    Blazzer/Jackets
                  </Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
