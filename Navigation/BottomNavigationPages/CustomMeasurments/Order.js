import React, { Component } from "react";
import {
  Switch,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Image
} from "react-native";
import { Header, Left, Right, Body } from "native-base";
import * as Animatable from "react-native-animatable";
import Accordion from "react-native-collapsible/Accordion";

const C1 = "78A643CD409	August 08, 2017	In Progress	$760.50";
const C2 = "34VB5540K83	July 21, 2017	Delayed	$315.20";
const C3 = "28BA67U0981	May 19, 2017	Delivered	$198.35";
const C4 = "28BA67U0981	May 19, 2017	Cancel	$198.35";

const CONTENT = [
  {
    title: "Coat",
    content: C1
  },
  {
    title: "Shirt",
    content1: C2
  },
  {
    title: "Pents",
    content2: C3
  },
  {
    title: "Casual Shirts",
    content3: C4
  }
];

const SELECTORS = [
  {
    title: "InProgess",
    value: 0
  },
  {
    title: "Delayed",
    value: 1
  },
  {
    title: "Delivered",
    value: 2
  },
  {
    title: "Canceled",
    value: 3
  }
];

export default class Order extends Component {
  static navigationOptions = {
    header: null
  };
  state = {
    activeSections: [],
    collapsed: true,
    multipleSelect: false
  };

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  setSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections
    });
  };

  renderHeader = (section, _, isActive) => {
    return (
      <Animatable.View
        duration={400}
        style={[styles.header, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
        <Text style={styles.headerText}>{section.title}</Text>
      </Animatable.View>
    );
  };

  renderContent(section, _, isActive) {
    return (
      <View>
        <Animatable.View
          duration={400}
          style={[styles.content, isActive ? styles.active : styles.inactive]}
          transition="backgroundColor"
        >
          <Animatable.View
            animation={isActive ? "bounceIn" : undefined}
            style={{
              borderWidth: 1,
              borderRadius: 2,
              borderColor: "grey",
              elevation: 3
            }}
          >
            <View
              style={{
                flexDirection: "row",
                height: 120
              }}
            >
              <Image
                source={require("../../../img/3.jpg")}
                style={{
                  width: 100,
                  height: 110,
                  marginLeft: 15,
                  marginTop: 5
                }}
              />
              <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15 }}>
                <Text
                  style={{
                    color: "red",

                    fontWeight: "bold",
                    fontSize: 15
                  }}
                >
                  ID: 78A643
                </Text>
                <Text
                  style={{
                    color: "black",

                    fontWeight: "bold",
                    fontSize: 15
                  }}
                >
                  Date: Aug 08, 2017{" "}
                </Text>
                <Text
                  style={{
                    color: "#15a7ee",

                    fontWeight: "bold",
                    fontSize: 15
                  }}
                >
                  Status: In Progress
                </Text>
                <Text
                  style={{
                    color: "black",
                    fontWeight: "bold",

                    fontSize: 15
                  }}
                >
                  Price: $760.50
                </Text>
              </View>
            </View>
          </Animatable.View>
        </Animatable.View>
      </View>
    );
  }

  render() {
    const { multipleSelect, activeSections } = this.state;

    return (
      <View style={{ backgroundColor: "white", flex: 1 }}>
        <ScrollView>
          <View
            style={{
              height: 0.3,
              borderWidth: 0.3,
              borderColor: "white",
              width: "100%"
            }}
          />
          <Header
            style={{
              backgroundColor: "#15a7ee",
              marginBottom: 20
            }}
          >
            <StatusBar backgroundColor="#15a7ee" barStyle="light-content" />
            <Left />

            <Body>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: "bold",
                  color: "white",
                  width: 200,
                  marginLeft: 40
                }}
              >
                Orders
              </Text>
            </Body>
          </Header>

          <View
            style={{
              flexDirection: "row",
              backgroundColor: "white",
              justifyContent: "center",
              alignContent: "center"
            }}
          >
            {SELECTORS.map(selector => (
              <TouchableOpacity
                key={selector.title}
                onPress={() => this.setSections([selector.value])}
              >
                <View
                  style={{
                    backgroundColor: "#15a7ee",
                    width: 65,
                    height: 30,
                    marginLeft: 5,
                    borderRadius: 5,
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      fontWeight: "bold",
                      paddingTop: 3,
                      paddingLeft: 5,
                      justifyContent: "center",
                      alignItems: "center",
                      fontSize: 12
                    }}
                  >
                    {selector.title}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
          <View
            style={{
              height: 0.5,
              width: width,
              borderWidth: 0.5,
              borderColor: "grey",
              marginTop: 15
            }}
          />

          <Accordion
            activeSections={activeSections}
            sections={CONTENT}
            touchableComponent={TouchableOpacity}
            expandMultiple={multipleSelect}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSections}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    // textAlign: 'center',
    color: "black",
    fontSize: 22,
    fontWeight: "300",
    marginBottom: 20
  },
  header: {
    backgroundColor: "white",
    padding: 10,
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 2,
    height: 80,
    elevation: 3
  },
  headerText: {
    // textAlign: 'center',
    color: "black",
    fontSize: 16,
    fontWeight: "500",
    paddingTop: 20
  },
  content: {
    padding: 5,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    elevation: 3,
    marginLeft: 5,
    marginTop: 3,
    borderWidth: 1,
    borderColor: "#e8eaed"
  },
  active: {
    backgroundColor: "white"
  },
  inactive: {
    backgroundColor: "white"
  },
  selectors: {
    marginBottom: 10,
    flexDirection: "row",
    // justifyContent: 'center',
    color: "black",
    marginRight: 5
  },
  selector: {
    backgroundColor: "white",
    padding: 5
  },
  selectTitle: {
    fontSize: 10,
    fontWeight: "500"
  },
  multipleToggle: {
    flexDirection: "row",
    justifyContent: "center"
    // marginVertical: 30,
    // alignItems: 'center',
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8
  }
});
