import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  ImageBackground,
  Image,
  ScrollView,
  FlatList,
  TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import IconGa from "react-native-vector-icons/MaterialIcons";
import IconAa from "react-native-vector-icons/AntDesign";
import IconPa from "react-native-vector-icons/FontAwesome";
import IconSa from "react-native-vector-icons/Entypo";
//import { ScrollView } from 'react-native-gesture-handler';

export default class HulaPokeFood extends Component {
  constructor(props) {
    super(props);

    this.state = {
      FlatListItems: [
        { key: "Location", rating: "9.7" },
        { key: "Price", rating: "9.7" },
        { key: "Quality", rating: "9.7" },
        { key: "Service", rating: "9.7" },
        { key: "Space", rating: "9.7" }
      ]
    };
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{ height: 0.5, width: "100%", backgroundColor: "#e8eaed" }}
      />
    );
  };

  GetItem(item) {
    Alert.alert(item);
  }

  static navigationOptions = {
    header: null
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          <View
            style={{
              height: hp("65%"),
              width: wp("100%"),
              elevation: 3,
              borderColor: "#e8eaed"
            }}
          >
            <ImageBackground
              style={{ width: "100%", height: hp("40%") }}
              source={require("../../img/5.jpg")}
              imageStyle={{ borderRadius: 5 }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginLeft: 10,
                  marginRight: 10,
                  marginTop: 20
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                >
                  <IconPa
                    name="chevron-left"
                    size={15}
                    style={{ color: "white", marginLeft: 20 }}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <IconSa
                    name="dots-three-horizontal"
                    size={15}
                    style={{ color: "white", marginRight: 20 }}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{ justifyContent: "flex-end", alignItems: "center" }}
              >
                <View
                  style={{
                    position: "absolute",
                    width: wp("15%"),
                    height: hp("9%"),
                    borderColor: "white",
                    borderRadius: 30,
                    alignItems: "center",
                    justifyContent: "center",
                    top: 205
                  }}
                >
                  <Image
                    source={require("../../img/5.jpg")}
                    style={{
                      width: wp("15%"),
                      height: hp("9%"),
                      borderColor: "white",
                      borderRadius: 30,
                      borderWidth: 2,
                      backgroundColor: "white"
                    }}
                  />
                </View>
              </View>
            </ImageBackground>
            <View
              style={{
                marginTop: 40,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text style={{ fontWeight: "bold", color: "black" }}>
                {" "}
                The Museo Nacional de Anto
              </Text>
            </View>
            <View
              style={{
                width: "100%",
                height: 1,
                borderColor: "#e8eaed",
                borderWidth: 0.5,
                marginTop: 8,
                flexDirection: "row"
              }}
            />
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                marginTop: 20
              }}
            >
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Icon name="heart-o" size={25} style={{ color: "grey" }} />
                <Text>WishList</Text>
              </View>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <IconAa
                  name="shoppingcart"
                  size={25}
                  style={{ color: "grey" }}
                />
                <Text>Add to cart</Text>
              </View>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <IconPa name="cut" size={25} style={{ color: "grey" }} />
                <Text>Customize</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              elevation: 3,
              marginLeft: 5,
              marginTop: 3,
              borderColor: "#e8eaed",
              height: hp("40%"),
              width: wp("97%"),
              marginBottom: 10
            }}
          >
            <View
              style={{
                flexDirection: "row",
                paddingTop: 10,
                paddingLeft: 10,
                marginTop: 5
              }}
            >
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: "bold",
                  color: "black",
                  marginLeft: 5
                }}
              >
                Description
              </Text>
            </View>
            <View
              style={{
                width: "100%",
                height: 1,
                borderColor: "#e8eaed",
                borderWidth: 0.5,
                marginTop: 15,
                flexDirection: "row"
              }}
            />
            <View style={{ marginTop: 10 }}>
              <Text style={{ marginLeft: 10, marginRight: 10 }}>
                it is a symbol of identity and mentor for generations , it is a
                symbol of identity and mentor for generation it is a symbol of
                identity and mentor for generations , it is a symbol of identity
                and mentor for generation it is a symbol of identity and mentor
                for generations , it is a symbol of identity and mentor for
                generation it is a symbol of identity and mentor for generations
                , it is a symbol of identity and mentor for generation it is a
                symbol of identity and mentor for generations , it is a symbol
                of identity and mentor for generation
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    padding: 10,
    //fontSize: 20,
    height: 40
  }
});
