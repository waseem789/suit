import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity,
  StyleSheet,
  TextInput
} from "react-native";
import { Header, Left, Right, Body, Title } from "native-base";
import IconGa from "react-native-vector-icons/MaterialIcons";
import IconAa from "react-native-vector-icons/AntDesign";
import IconPa from "react-native-vector-icons/FontAwesome5";
import IconSa from "react-native-vector-icons/Entypo";
export default class Wish extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        {/* <View
          style={{
            height: 0.3,
            borderWidth: 0.3,
            borderColor: "white",
            width: "100%"
          }}
        /> */}
        <Header
          style={{
            backgroundColor: "#15a7ee"
          }}
        >
          <StatusBar backgroundColor="#007bb7" barStyle="light-content" />

          <Left>
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                color: "white",
                width: 200,
                marginLeft: 40
              }}
            >
              Cart
            </Text>
          </Left>
        </Header>
        <View
          style={{
            flexDirection: "row",
            marginTop: 20,
            backgroundColor: "white"
          }}
        >
          <Image
            source={require("../../img/4.jpg")}
            style={{ width: 50, height: 50, marginRight: 10 }}
          />

          <TouchableOpacity>
            <View>
              <Text style={styles.text}>Branded</Text>
              <Text style={styles.normalText}>Price: $200</Text>
            </View>
          </TouchableOpacity>
          <View style={{ flexDirection: "row", marginLeft: 90, marginTop: 10 }}>
            <IconAa
              name="plus"
              size={15}
              style={{ color: "red", marginTop: 13 }}
            />
            <TextInput
              placeholder="1"
              textAlign={"center"}
              style={{
                height: 35,
                borderWidth: 1,
                margin: 5,
                borderColor: "grey",
                width: 50
              }}
            />
            <IconAa
              name="minus"
              size={15}
              style={{ color: "green", marginTop: 13 }}
            />
            <IconSa
              name="cross"
              size={15}
              style={{ color: "red", marginLeft: 20, marginTop: 13 }}
            />
          </View>
        </View>
        <View
          style={{
            height: 0.2,
            width: width,
            borderWidth: 0.2,
            borderColor: "grey"
          }}
        />

        <View
          style={{
            flexDirection: "row",
            marginTop: 10,
            backgroundColor: "white"
          }}
        >
          <Image
            source={require("../../img/4.jpg")}
            style={{ width: 50, height: 50, marginRight: 10, marginLeft: 10 }}
          />
          <TouchableOpacity>
            <View>
              <Text style={styles.text}>Branded</Text>
              <Text style={styles.normalText}>Price: $200</Text>
            </View>
          </TouchableOpacity>
          <View style={{ flexDirection: "row", marginLeft: 80, marginTop: 10 }}>
            <IconAa
              name="plus"
              size={15}
              style={{ color: "red", marginTop: 13 }}
            />
            <TextInput
              placeholder="1"
              textAlign={"center"}
              style={{
                height: 35,
                borderWidth: 1,
                margin: 5,
                borderColor: "grey",
                width: 50
              }}
            />
            <IconAa
              name="minus"
              size={15}
              style={{ color: "green", marginTop: 13 }}
            />
            <IconSa
              name="cross"
              size={15}
              style={{ color: "red", marginLeft: 20, marginTop: 13 }}
            />
          </View>
        </View>
        <View
          style={{
            height: 0.2,
            width: width,
            borderWidth: 0.2,
            borderColor: "grey"
          }}
        />

        <View
          style={{
            flexDirection: "row",
            marginTop: 10,
            backgroundColor: "white"
          }}
        >
          <Image
            source={require("../../img/5.jpg")}
            style={{ width: 50, height: 50, marginRight: 10, marginLeft: 10 }}
          />
          <TouchableOpacity>
            <View>
              <Text style={styles.text}>Branded</Text>
              <Text style={styles.normalText}>Price: $200</Text>
            </View>
          </TouchableOpacity>
          <View style={{ flexDirection: "row", marginLeft: 80, marginTop: 10 }}>
            <IconAa
              name="plus"
              size={15}
              style={{ color: "red", marginTop: 13 }}
            />
            <TextInput
              placeholder="1"
              textAlign={"center"}
              style={{
                height: 35,
                borderWidth: 1,
                margin: 5,
                borderColor: "grey",
                width: 50
              }}
            />
            <IconAa
              name="minus"
              size={15}
              style={{ color: "green", marginTop: 13 }}
            />
            <IconSa
              name="cross"
              size={15}
              style={{ color: "red", marginLeft: 20, marginTop: 13 }}
            />
          </View>
        </View>
        <View
          style={{
            height: 0.2,
            width: width,
            borderWidth: 0.2,
            borderColor: "grey"
          }}
        />

        <View
          style={{
            flexDirection: "row",
            marginTop: 10,
            backgroundColor: "white"
          }}
        >
          <Image
            source={require("../../img/4.jpg")}
            style={{ width: 50, height: 50, marginRight: 10, marginLeft: 10 }}
          />
          <TouchableOpacity>
            <View>
              <Text style={styles.text}>Branded</Text>
              <Text style={styles.normalText}>Price: $200</Text>
            </View>
          </TouchableOpacity>
          <View style={{ flexDirection: "row", marginLeft: 80, marginTop: 10 }}>
            <IconAa
              name="plus"
              size={15}
              style={{ color: "red", marginTop: 13 }}
            />
            <TextInput
              placeholder="1"
              textAlign={"center"}
              style={{
                height: 35,
                borderWidth: 1,
                margin: 5,
                borderColor: "grey",
                width: 50
              }}
            />
            <IconAa
              name="minus"
              size={15}
              style={{ color: "green", marginTop: 13 }}
            />
            <IconSa
              name="cross"
              size={15}
              style={{ color: "red", marginLeft: 20, marginTop: 13 }}
            />
          </View>
        </View>
        <View
          style={{
            height: 0.2,
            width: width,
            borderWidth: 0.2,
            borderColor: "grey"
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 20,
    margin: 15
  },
  text: {
    color: "black",
    fontWeight: "bold",
    fontSize: 15,
    marginLeft: 20,
    marginTop: 10
  },
  normalText: {
    color: "black",
    fontSize: 10,
    marginLeft: 20,
    marginTop: 8,
    marginBottom: 15
  }
});
