import React, { Component } from "react";
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  ImageBackground,
  Image,
  StatusBar,
  TouchableOpacity
} from "react-native";
const SCREEN_Width = Dimensions.get("window").width;
import Icon from "react-native-vector-icons/FontAwesome";
import Iconua from "react-native-vector-icons/Ionicons";

export default class Home extends Component {
  render() {
    return (
      <ScrollView>
        <StatusBar backgroundColor="#007bb7" barStyle="light-content" />
        <View>
          <View>
            <Image
              source={require("../../img/banner.jpg")}
              style={{ height: 200, width: SCREEN_Width }}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20
            }}
          >
            <Text style={{ color: "black", fontWeight: "bold", fontSize: 20 }}>
              Top Categories
            </Text>
          </View>
          <View>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              horizontal="true"
            >
              <View style={{ flexDirection: "row", width: "49.5%" }}>
                <View
                  style={{
                    height: 200,
                    width: "49.5%",
                    marginTop: 10,
                    marginRight: 0,
                    paddingRight: 5,
                    borderWidth: 0.5,
                    marginLeft: 10,
                    backgroundColor: "white",
                    borderColor: "grey",
                    borderRadius: 5
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("Categories")}
                  >
                    <ImageBackground
                      style={{
                        width: 165,
                        padding: 10,
                        height: 120,
                        marginTop: 0,
                        marginRight: 5
                      }}
                      source={require("../../img/1.jpg")}
                      imageStyle={{ borderRadius: 5 }}
                    >
                      <View
                        style={{
                          position: "absolute",
                          width: 40,
                          height: 40,
                          borderRadius: 20,
                          backgroundColor: "white",
                          left: 8,
                          top: 100,
                          alignItems: "center",
                          justifyContent: "center"
                        }}
                      >
                        <Icon
                          name="shopping-bag"
                          size={25}
                          style={{ color: "#15a7ee", borderRadius: 20 }}
                        />
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                  <Text
                    style={{
                      paddingLeft: 12,
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold",
                      paddingTop: 30
                    }}
                  >
                    Blazzers/Jackets
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      height: 1,
                      borderColor: "#d0dbd6",
                      borderWidth: 0.5,
                      marginTop: 3,
                      flexDirection: "row"
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ color: "black" }}>Price:</Text>
                      <Text>$200</Text>
                    </View>
                    <Text style={{ color: "#15a7ee" }}>Trending</Text>
                  </View>
                </View>

                <View
                  style={{
                    height: 200,
                    width: "49.5%",
                    backgroundColor: "white",
                    marginTop: 10,
                    marginRight: 0,
                    paddingRight: 5,
                    borderColor: "grey",
                    borderWidth: 0.5,
                    marginLeft: 2,
                    borderRadius: 5
                  }}
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/2.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="shopping-bag"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                  <Text
                    style={{
                      paddingLeft: 12,
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold",
                      paddingTop: 30
                    }}
                  >
                    Suits
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      height: 1,
                      borderColor: "#d0dbd6",
                      borderWidth: 0.5,
                      marginTop: 3,
                      flexDirection: "row"
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ color: "black" }}>Price:</Text>
                      <Text>$200</Text>
                    </View>
                    <Text style={{ color: "#15a7ee" }}>Trending</Text>
                  </View>
                </View>
                <View
                  style={{
                    height: 200,
                    width: "49.5%",
                    backgroundColor: "white",
                    marginTop: 10,
                    marginRight: 0,
                    paddingRight: 5,
                    borderColor: "grey",
                    borderWidth: 0.5,
                    marginLeft: 2,
                    borderRadius: 5
                  }}
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/2.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="shopping-bag"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                  <Text
                    style={{
                      paddingLeft: 12,
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold",
                      paddingTop: 30
                    }}
                  >
                    Pants
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      height: 1,
                      borderColor: "#d0dbd6",
                      borderWidth: 0.5,
                      marginTop: 3,
                      flexDirection: "row"
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ color: "black" }}>Price:</Text>
                      <Text>$200</Text>
                    </View>
                    <Text style={{ color: "#15a7ee" }}>Trending</Text>
                  </View>
                </View>
                <View
                  style={{
                    height: 200,
                    width: "47%",
                    backgroundColor: "white",
                    marginTop: 10,
                    marginRight: 0,
                    paddingRight: 5,
                    borderColor: "grey",
                    borderWidth: 0.5,
                    marginLeft: 2,
                    borderRadius: 5,
                    position: "relative"
                  }}
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/1.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="shopping-bag"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                  <Text
                    style={{
                      paddingLeft: 12,
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold",
                      paddingTop: 30
                    }}
                  >
                    Shirts
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      height: 1,
                      borderColor: "#d0dbd6",
                      borderWidth: 0.5,
                      marginTop: 3,
                      flexDirection: "row"
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ color: "black" }}>Price:</Text>
                      <Text>$200</Text>
                    </View>
                    <Text style={{ color: "#15a7ee" }}>Trending</Text>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20
            }}
          />
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontSize: 20, color: "black", fontWeight: "bold" }}>
              Best Deals
            </Text>
          </View>
          <View>
            <Image
              source={require("../../img/banner2.jpg")}
              style={{ height: 200, width: SCREEN_Width }}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20
            }}
          >
            <Text style={{ color: "black", fontWeight: "bold", fontSize: 20 }}>
              Products
            </Text>
          </View>
          <View>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              style={{ marginRight: 10 }}
            >
              <View style={{ flexDirection: "row", width: "49.5%" }}>
                <View
                  style={{
                    height: 200,
                    width: "49%",
                    backgroundColor: "white",
                    marginTop: 10,
                    marginRight: 0,
                    paddingRight: 5,
                    borderColor: "grey",
                    borderWidth: 0.5,
                    marginLeft: 10,
                    borderRadius: 5
                  }}
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/casual.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="shopping-bag"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                  <Text
                    style={{
                      paddingLeft: 12,
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold",
                      paddingTop: 30
                    }}
                  >
                    Casual Shirts
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      height: 1,
                      borderColor: "#d0dbd6",
                      borderWidth: 0.5,
                      marginTop: 3,
                      flexDirection: "row"
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ color: "black" }}>Price:</Text>
                      <Text>$200</Text>
                    </View>
                    <Text style={{ color: "#15a7ee" }}>Trending</Text>
                  </View>
                </View>
                <View
                  style={{
                    height: 200,
                    width: "49%",
                    backgroundColor: "white",
                    marginTop: 10,
                    marginRight: 0,
                    paddingRight: 5,
                    borderColor: "grey",
                    borderWidth: 0.5,
                    marginLeft: 2,
                    borderRadius: 5
                  }}
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/coat.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="shopping-bag"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                  <Text
                    style={{
                      paddingLeft: 12,
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold",
                      paddingTop: 30
                    }}
                  >
                    Coats
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      height: 1,
                      borderColor: "#d0dbd6",
                      borderWidth: 0.5,
                      marginTop: 3,
                      flexDirection: "row"
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ color: "black" }}>Price:</Text>
                      <Text>$200</Text>
                    </View>
                    <Text style={{ color: "#15a7ee" }}>Trending</Text>
                  </View>
                </View>
                <View
                  style={{
                    height: 200,
                    width: "49.5%",
                    backgroundColor: "white",
                    marginTop: 10,
                    marginRight: 0,
                    paddingRight: 5,
                    borderColor: "grey",
                    borderWidth: 0.5,
                    marginLeft: 2,
                    borderRadius: 5
                  }}
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/pent.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="shopping-bag"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                  <Text
                    style={{
                      paddingLeft: 12,
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold",
                      paddingTop: 30
                    }}
                  >
                    Pents
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      height: 1,
                      borderColor: "#d0dbd6",
                      borderWidth: 0.5,
                      marginTop: 3,
                      flexDirection: "row"
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ color: "black" }}>Price:</Text>
                      <Text>$200</Text>
                    </View>
                    <Text style={{ color: "#15a7ee" }}>Trending</Text>
                  </View>
                </View>
                <View
                  style={{
                    height: 200,
                    width: "49.5%",
                    backgroundColor: "white",
                    marginTop: 10,
                    marginRight: 0,
                    paddingRight: 5,
                    borderColor: "grey",
                    borderWidth: 0.5,
                    marginLeft: 2,
                    borderRadius: 5
                  }}
                >
                  <ImageBackground
                    style={{
                      width: 165,
                      padding: 10,
                      height: 120,
                      marginTop: 0,
                      marginRight: 5
                    }}
                    source={require("../../img/shirts.jpg")}
                    imageStyle={{ borderRadius: 5 }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        backgroundColor: "white",
                        left: 8,
                        top: 100,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Icon
                        name="shopping-bag"
                        size={25}
                        style={{ color: "#15a7ee", borderRadius: 20 }}
                      />
                    </View>
                  </ImageBackground>
                  <Text
                    style={{
                      paddingLeft: 12,
                      fontSize: 15,
                      color: "black",
                      fontWeight: "bold",
                      paddingTop: 30
                    }}
                  >
                    Formal Shirts
                  </Text>
                  <View
                    style={{
                      width: "100%",
                      height: 1,
                      borderColor: "#d0dbd6",
                      borderWidth: 0.5,
                      marginTop: 3,
                      flexDirection: "row"
                    }}
                  />
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text style={{ color: "black" }}>Price:</Text>
                      <Text>$200</Text>
                    </View>
                    <Text style={{ color: "#15a7ee" }}>Trending</Text>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </ScrollView>
    );
  }
}
