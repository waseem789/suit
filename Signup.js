import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
  ImageBackground
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

import axios from "axios";

const ACCESS_TOKEN = "access_token";

export default class Signup extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      password_confirmation: "",
      loading: false,
      token: "",
      hidePassword: true
    };
  }

  async storeToken() {
    const { token } = this.state;
    try {
      await AsyncStorage.setItem(ACCESS_TOKEN, token);
      console.log("Token Saved", token);
      //this.getToken();
    } catch (error) {
      console.log("Token cannot be saved");
    }
  }

  getTokenFromLoginRequest() {
    const { email, password, token } = this.state;

    axios
      .post("http://198.245.53.50:3000/api/customer/login", {
        email: email,
        password: password
      })
      .then(response => {
        console.log(response);
        this.setState({ token: response.data.token });
      })
      .then(console.log(token))
      .then(this.storeToken.bind(this));
  }

  //state = { fullName: "", email: "", password: "", loading: false };

  static navigationOptions = {
    header: null
  };
  onSignupButtonPress() {
    const {
      name,
      email,
      password,
      password_confirmation,

      loading
    } = this.state;
    // console.log(
    //   "email: ",
    //   email,
    //   "fullName: ",
    //   fullName,
    //   "password: ",
    //   password
    // );

    this.setState({ loading: true });

    if (password === password_confirmation) {
      if (password.length >= 6) {
        axios
          .post("https://customsuits.herokuapp.com/api/users/register", {
            name: name,
            email: email,
            password: password,
            password2: password_confirmation,
            role: "5c8a4f7ec302c00017877b24"
          })
          .then(response => console.log(response))
          .then(this.getTokenFromLoginRequest.bind(this))
          .then(this.onSignupSuccess.bind(this))
          .then(() => {
            this.props.navigation.navigate("Home");
          })
          .catch(this.onSignupFailure.bind(this));
      } else {
        alert("Password must be atleast 6 characters ");
        this.setState({ loading: false });
      }
    } else {
      alert("Password does not match");
      this.setState({ loading: false });
    }
  }
  onSignupSuccess() {
    this.setState({
      fullName: "",
      email: "",
      //phoneNumber: '',
      password: "",
      password_confirmation: "",
      loading: false
    });
  }
  toggleShowButton() {
    this.setState({ hidePassword: !this.state.hidePassword });
  }
  onSignupFailure() {
    this.setState({
      loading: false
    });
    alert("SignUp Failed");
  }
  renderButton() {
    if (this.state.loading) {
      return <ActivityIndicator size="large" color="#15a7ee" />;
    }
    return (
      <TouchableOpacity onPress={this.onSignupButtonPress.bind(this)}>
        <ImageBackground
          source={require("./img/createAccountButton.png")}
          style={{ width: 280, height: 50 }}
        />
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <ImageBackground
        style={{ backgroundColor: "white", width: "100%", height: "100%" }}
      >
        <View style={styles.container}>
          <Text style={styles.loginTextStyle}>Sign Up</Text>
        </View>
        <View style={styles.textInputStyle}>
          <View
            style={{
              flexDirection: "row",
              borderColor: "#15a7ee",
              borderWidth: 1,
              borderRadius: 25,
              height: 50,
              width: "80%",
              backgroundColor: "rgba(255, 255, 255, 0.3)"
              //opacity: 0.7
            }}
          >
            <Icon
              name="ios-contact"
              size={25}
              style={{
                color: "#15a7ee",
                paddingLeft: 15,
                paddingTop: 8,
                paddingRight: 10
              }}
            />
            <TextInput
              fontSize={20}
              placeholder="Full Name"
              autoCapitalize="words"
              autoCorrect={false}
              onChangeText={fullName => this.setState({ fullName })}
              value={this.state.fullName}
              placeholderTextColor="#15a7ee"
              style={{ color: "#15a7ee", height: "100%", width: "80%" }}
              // selectionColor="#15a7ee"
              //underlineColorAndroid="#15a7ee"
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              borderColor: "#15a7ee",
              borderWidth: 1,
              borderRadius: 25,
              height: 50,
              width: "80%",
              marginTop: 10,
              backgroundColor: "rgba(255, 255, 255, 0.3)"
            }}
          >
            <Icon
              name="ios-mail"
              size={25}
              style={{
                color: "#15a7ee",
                paddingLeft: 15,
                paddingTop: 8,
                paddingRight: 10
              }}
            />
            <TextInput
              fontSize={20}
              placeholder="example@gmail.com"
              autoCorrect={false}
              onChangeText={email => this.setState({ email })}
              value={this.state.email}
              placeholderTextColor="#15a7ee"
              style={{ color: "#15a7ee", height: "100%", width: "80%" }}
              // selectionColor="#15a7ee"
              // underlineColorAndroid="#15a7ee"
              autoCapitalize="none"
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              borderColor: "#15a7ee",
              borderWidth: 1,
              borderRadius: 25,
              height: 50,
              width: "80%",
              marginTop: 10,
              backgroundColor: "rgba(255, 255, 255, 0.3)"
            }}
          >
            <Icon
              name="ios-lock"
              size={25}
              style={{
                color: "#15a7ee",
                paddingLeft: 15,
                paddingTop: 8,
                paddingRight: 10
              }}
            />
            <TextInput
              fontSize={20}
              placeholder="Password"
              autoCorrect={false}
              secureTextEntry={true}
              onChangeText={password => this.setState({ password })}
              value={this.state.password}
              placeholderTextColor="#15a7ee"
              style={{ color: "#15a7ee", height: "100%", width: "80%" }}
              // selectionColor="#15a7ee"
              // underlineColorAndroid="#15a7ee"
              autoCapitalize="none"
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              borderColor: "#15a7ee",
              borderWidth: 1,
              borderRadius: 25,
              height: 50,
              width: "80%",
              marginTop: 10,
              backgroundColor: "rgba(255, 255, 255, 0.3)"
            }}
          >
            <Icon
              name="ios-lock"
              size={25}
              style={{
                color: "#15a7ee",
                paddingLeft: 15,
                paddingTop: 8,
                paddingRight: 10
              }}
            />
            <TextInput
              fontSize={20}
              placeholder="Confirm Password"
              autoCorrect={false}
              secureTextEntry={true}
              onChangeText={password_confirmation =>
                this.setState({ password_confirmation })
              }
              value={this.state.password_confirmation}
              placeholderTextColor="#15a7ee"
              style={{ color: "#15a7ee", height: "100%", width: "80%" }}
              // selectionColor="#15a7ee"
              // underlineColorAndroid="#15a7ee"
              autoCapitalize="none"
            />
          </View>
        </View>

        <View style={styles.buttonsViewStyle}>
          {this.renderButton()}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Login")}
            style={{ marginTop: 30 }}
          >
            <ImageBackground
              source={require("./img/alreadyHaveAnAccountButton.png")}
              style={{ width: 280, height: 50 }}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 100
  },
  textStyle: {
    fontSize: 12,
    fontWeight: "bold",
    color: "#15a7ee"
  },
  loginTextStyle: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#15a7ee"
  },
  input: {
    backgroundColor: "#15a7ee",
    width: 40,
    height: 40,
    marginHorizontal: 20,
    paddingLeft: 45,
    borderRadius: 20,
    color: "#15a7ee"
  },
  showButton: {
    position: "absolute",
    right: 30,
    marginTop: 130
  },
  textInputStyle: {
    alignItems: "center",
    flexDirection: "column",
    marginTop: 80
  },
  forgotCreateStyle: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20
  },
  buttonsViewStyle: {
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: 20
  }
});
